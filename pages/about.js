import Layout from "../components/Layout";
import background from "../public/img/background.jpg"

const About = () => {
    return (
        <div className="" >
            <h1 className="flex justify-center text-blue-500 mt-6 pb-5 border-blue-300 border-b-2 lg:text-3xl text-bold">
                About Us
            </h1>
            <p className="text-gray-600 pt-8 justify-center items-center flex" >
                Food security is the measure of the availability of food and individuals' ability to access it.<br />
                According to the United Nations' Committee on World Food Security, food security is defined as all people,<br />
                at all times, have physical, social, and economic access to sufficient, safe, and nutritious food that meets their<br />
                food preferences and dietary needs for an active and healthy life. Food insecurity has been a major issue throughout history<br />
                and it continues to be a problem today. However, not many people in germany are aware of the food deserts and lack of access people have to<br />
                sufficient nutrition in their own communities.<br /><br />

                For our Hackathon project, we created a website where people can learn about food accessibility in their communities by entering their Address. With our site,<br />
                we are trying to educate people about food accessibility around them and urging them to contribute in making an easy supply of<br />
                nutritious food a reality for everyone.<br />
                We have compiled a list of non-profit organizations for people to support.<br />
                These organizations are working diligently to ensure food security for all.<br />
                This shows people how their donation can make an impact in people's daily lives and overall health in communities far away and close to home.<br /><br />

                Team Name: Anti waste<br /><br />

                Members: Denni, Andy, Ethan
            </p>
        </div>
    );
};

export default About;