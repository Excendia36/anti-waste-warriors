import { connectToDatabase } from "../util/mongodb";

export default function Food({ food }) {
  return (
    <div>
          <p>{JSON.stringify(food)}</p>
    </div>
  );
}

function timeConverter(UNIX_timestamp) {
  var a = new Date(UNIX_timestamp * 1000);
  var months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time =
    date + " " + month + " " + year + " " + hour + ":" + min + ":" + sec;
  return time;
}

export async function getServerSideProps() {
  const { db } = await connectToDatabase();

  const food = await db.collection("food").find({}).limit(20).toArray();

  return {
    props: {
      food: JSON.parse(JSON.stringify(food)),
    },
  };
}
