import Layout from "../components/Layout";
import React, { useState } from "react";
import Router from "next/router";
import cookie from "js-cookie";
import Link from "next/link";
import useSWR from "swr";
import NewPost from "../components/NewPost";

function Profile() {
  const { data, revalidate } = useSWR("/api/cookie_handler", async function (args) {
    const res = await fetch(args);
    return res.json();
  });
  if (!data) return <h1>Loading...</h1>;
  let loggedIn = false;
  if (data.email) {
    loggedIn = true;
  }
  return (
    <div className="">
      <div>
        <h3> My Information</h3>
        <h3> Create a Post</h3>
      </div>
        <div>
            <NewPost/>
        </div>
      <h1 className="flex justify-center text-blue-500 mt-6 pb-5 border-blue-300 border-b-2 lg:text-3xl text-bold">
        {loggedIn && (
          <>
            <button
              onClick={() => {
                cookie.remove("token");
                revalidate();
              }}
            >
              Logout
            </button>
          </>
        )}
        {!loggedIn && <p> Please Register or Login First</p>}
      </h1>


    </div>
  );
}

export default Profile;
