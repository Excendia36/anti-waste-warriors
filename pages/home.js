import Layout from "../components/Layout";
import Link from "next/link";
import Image from "next/image";
import { Flex, Box, Text, Button } from "@chakra-ui/react";
import Post from "../components/Post";
import useSWR from "swr";

export async function getStaticProps(context) {
  const res = await fetch("http://localhost:3000/api/food");
  const data = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: { data }, // will be passed to the page component as props
  };
}

function Home({ data }) {
  return (
    <>
      <h1 className="flex justify-center text-blue-500 mt-6 pb-5 border-blue-300 border-b-2 lg:text-3xl text-bold">
        Welcome to Charity Page
      </h1>
      <Box>
        <Flex flexWrap="wrap">
          {data.map((post) => (
            <Post post={post}/>
          ))}
        </Flex>
      </Box>
    </>
  );
}

export default Home;
