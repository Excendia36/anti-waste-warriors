import { connectToDatabase } from "../../../../util/mongodb";

export default async (req, res) => {
  const { db } = await connectToDatabase();
  var food = db.collection("food").find().toArray();
  if (req.method === 'GET') {
    food = await db.collection("food").find({ "id_value": req.query.id }).toArray();
  } else if (req.method === 'DELETE') {
    food = await db.collection("food").deleteOne({ "id_value": req.query.id });
  }else {
    // Handle any other HTTP method
    food = null;
    res.statusCode = 401;
    res.end();
  } 
  res.json(food);

};
