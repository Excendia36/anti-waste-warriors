const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const bcrypt = require('bcrypt');
const v4 = require('uuid').v4;
const jwt = require('jsonwebtoken');
const jwtSecret = 'SUPERSECRETE20220';

const saltRounds = 10;
const url = process.env.MONGODB_URI;
const dbName = process.env.MONGODB_DB;

const client = new MongoClient(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

function findUser(db, email, callback) {
  const collection = db.collection('user');
  collection.findOne({email}, callback);
}

function createUser(db,first_name,last_name,email, password,street,house_number,city,country,user_type,callback) {
  const collection = db.collection('user');
  bcrypt.hash(password, saltRounds, function(err, hash) {
    // Store hash in your password DB.
    collection.insertOne(
      {
        userId: v4(),
        first_name,
        last_name,
        email,
        password: hash,
        street,
        house_number,
        city,
        country,
        user_type
      },
      function(err, userCreated) {
        assert.equal(err, null);
        callback(userCreated);
      },
    );
  });
}

export default (req, res) => {
  if (req.method === 'POST') {
    // signup
    try {
      assert.notEqual(null, req.body.first_name, 'First name required');
      assert.notEqual(null, req.body.last_name, 'Last name required');
      assert.notEqual(null, req.body.email, 'Email required');
      assert.notEqual(null, req.body.password, 'Password required');
      assert.notEqual(null, req.body.street, 'Street name required');
      assert.notEqual(null, req.body.house_number, 'House number name required');
      assert.notEqual(null, req.body.city, 'City name required');
      assert.notEqual(null, req.body.country, 'Country name required');
      assert.notEqual(null, req.body.user_type, 'User type required');
    } catch (bodyError) {
      res.status(403).json({error: true, message: bodyError.message});
    }

    // verify email does not exist already
    client.connect(function(err) {
      assert.equal(null, err);
      console.log('Connected to MongoDB server =>');
      const db = client.db(dbName);
      const first_name=req.body.first_name;
      const last_name=req.body.last_name;
      const email = req.body.email;
      const password = req.body.password;
      const street=req.body.street;
      const house_number=req.body.house_number;
      const city=req.body.city;
      const country=req.body.country;
      const user_type=req.body.user_type;

      findUser(db, email, function(err, user) {
        if (err) {
          res.status(500).json({error: true, message: 'Error finding User'});
          return;
        }
        if (!user) {
          // proceed to Create
          createUser(db,first_name,last_name,email, password,street,house_number,city,country,user_type,function(creationResult) {
            if (creationResult.ops.length === 1) {
              const user = creationResult.ops[0];
              const token = jwt.sign(
                {userId: user.userId, email: user.email},
                jwtSecret,
                {
                  expiresIn: 3000, //50 minutes
                },
              );
              res.status(200).json({token});
              return;
            }
          });
        } else {
          // User exists
          res.status(403).json({error: true, message: 'Email exists'});
          return;
        }
      });
    });
  } else if(req.method === 'GET'){
    return res.status(200).json;
  }else {
    // Handle any other HTTP method
    res.status(200).json({users: ['Hello world!']});
  }
};
