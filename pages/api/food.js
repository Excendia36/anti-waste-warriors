const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const bcrypt = require('bcrypt');
const v4 = require('uuid').v4;
const jwt = require('jsonwebtoken');
const jwtSecret = 'SUPERSECRETE20220';

const saltRounds = 10;
const url = process.env.MONGODB_URI;
const dbName = process.env.MONGODB_DB;

const client = new MongoClient(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

async function findFoodAll(db, res) {
  const food = await db.collection("food").find({}).toArray();
  res.json(food);
}

function createFoodPost(db, title, description, image, email,res) {
  const collection = db.collection('food');
  collection.insertOne(
    {
      id_value: v4(),
      email,
      title,
      description,
      created:Date.now(),
      image
    }
  );
  res.status(203);
}

export default (req, res) => {
  if (req.method === 'POST') {
    try {
      assert.notEqual(null, req.body.title, 'Title required');
      assert.notEqual(null, req.body.description, 'Description required');
      assert.notEqual(null, req.body.email, 'Email required');
    } catch (bodyError) {
      res.status(403).json({ error: true, message: bodyError.message });
    }
    client.connect(function (err) {
      const email = req.body.email;
      const title = req.body.title;
      const description = req.body.description;
      const image = req.body.image;

      const db = client.db(dbName);
      createFoodPost(db, title, description, image, email,res);
    });

  } else if (req.method === 'GET') {
    client.connect(function (err) {
      const db = client.db(dbName);
      findFoodAll(db, res)
    });
  } else {
    // Handle any other HTTP method
    res.status(200).json({users: ['Hello world!']});
  }
};
