module.exports = {
    purge: [],
    theme: {
        extend: {
            backgroundImage: {
              'Signup-Background': "url('/img/Signup-Background.jpeg')",
                'background-image': "url(/img/background.jpg)",
            }
        },
    },
    variants: {},
    plugins: [require('@tailwindcss/custom-forms'),],
}
