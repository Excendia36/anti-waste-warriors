import Head from "next/head";
import Navbar from "./Navbar";

const Layout = ({children}) => {
    return (
        <>
            <Head>
                <title>Anti Waste</title>
            </Head>
            <header>
                <Navbar />
            </header>
            <main>
                {children}
            </main>
            <footer className="w-full fixed bottom-0 text-center border-t bg-gradient-to-r from-purple-400 to-green-300 text-white p-4 pin-b">
                <span className="lg:text-xl"> Denni, Andy, Ethan</span>
            </footer>
        </>
    );
};

Layout.defaultProps = {
    metaInfo: {
        title: "Default Title",
        metaKeywords: "Default metaKeywords",
        metaDesc: "Default metaDesc",
    },
};

export default Layout;
