import React, { useState } from "react";
import Router from "next/router";
import cookie from "js-cookie";
import Link from "next/link";
import useSWR from "swr";


function NewPost() {
    const [cardError, setCardError] = useState("");
    const [email, setEmail] = useState("");
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [image, setImage] = useState("");

    const {data, revalidate} = useSWR('/api/cookie_handler', async function(args) {
        const res = await fetch(args);
        return res.json();
      });
      if (!data) return <h1>Loading...</h1>;
      let loggedIn = false;
      if (data.email) {
        loggedIn = true;
      }

    function checkEmpty(input) {
        if (input.length < 1) return true;
        return false;
    }
    function handleSubmit(e) {
        setEmail(data.email)
        if (checkEmpty(title)) return;
        if (checkEmpty(description)) return;

        e.preventDefault();
        fetch("/api/food", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                email,
                title,
                description,
                image,
            }),
        })
            .then((r) => r.json())
            Router.push("/")
    }
    return (
        <form onSubmit={handleSubmit}>
            <div className="pt-24 flex flex-col items-center justify-center border-5">
                <div className="w-full max-w-xs">
                    <div className="mt-1 flex items-center">
                {/*<span className="inline-block h-12 w-12 rounded-full overflow-hidden bg-gray-100">*/}
                {/*  <svg className="h-full w-full text-gray-300" fill="currentColor" viewBox="0 0 24 24">*/}
                {/*    <path*/}
                {/*        d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z"/>*/}
                {/*  </svg>*/}
                {/*</span>*/}
                    </div>
                    <div className="mb-4">
                        <label
                            className="block text-gray-700 text-sm font-bold mb-2"
                        >
                            Title
                        </label>
                        <input
                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            id="title"
                            placeholder="title"
                            name="title"
                            required={true}
                            onChange={(e) => setTitle(e.target.value)}
                        />
                    </div>
                    <div className="mb-4">
                        <label
                            className="block text-gray-700 text-sm font-bold mb-2"
                        >
                            Description
                        </label>
                        <textarea
                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            id="title"
                            placeholder="title"
                            name="title"
                            required={true}
                            onChange={(e) => setDescription(e.target.value)}
                        ></textarea>
                    </div>
                    <div className="flex items-center justify-between">
                        <button
                            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            type="submit"
                        >
                            Create a Card
                        </button>
                        {cardError && <p style={{color: 'red'}}>{cardError}</p>}
                    </div>
                </div>
            </div>
        </form>
    );
}

export default NewPost;
