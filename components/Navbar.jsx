import { useState } from "react";
import Link from "next/link";
import Head from "next/head";
import fetch from "isomorphic-unfetch";
import useSWR from "swr";
import cookie from "js-cookie";

const Navbar = () => {
    const [showNav, setShowNav] = useState(false);
    const {data, revalidate} = useSWR('/api/cookie_handler', async function(args) {
        const res = await fetch(args);
        return res.json();
      });
      if (!data) return <h1>Loading...</h1>;
      let loggedIn = false;
      if (data.email) {
        loggedIn = true;
      }

  return (
    <header className="flex items-center p-3 flex-wrap text-white bg-gradient-to-r from-purple-400 to-green-300 ">
      <div
        id="logo"
        className="lg:text-xl p-2  justify-center items-center font-serif"
      >
        <Link href="/home">
          <a>Anti waste warriors</a>
        </Link>
      </div>
      <button
        onClick={() => setShowNav(!showNav)}
        type="button"
        className="inline-flex p-3 text-white hover:text-gray-300 focus:text-white focus:outline-none lg:hidden ml-auto"
      >
        <svg
          className="h-6 w-6 fill-current"
          viewBox="0 -53 384 384"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0" />
          <path d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0" />
          <path d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0" />
        </svg>
      </button>

      <div className="w-full flex-grow lg:inline-flex lg:flex-grow lg:w-auto">
        <div
          className={
            "lg:inline-flex lg:flex-row lg:ml-auto flex flex-col " +
            (showNav ? "" : "hidden")
          }
        >
          <Link href="/home">
            <a className="lg:inline-flex lg:w-auto px-3 py-2 rounded hover:bg-blue-200 hover:bg-gray-900">
              Home
            </a>
          </Link>

          <Link href="/about">
            <a className="lg:inline-flex lg:w-auto px-3 py-2 rounded hover:bg-blue-300 hover:bg-gray-900">
              About
            </a>
          </Link>
          <Link href="/contact">
            <a className="lg:inline-flex lg:w-auto px-3 py-2 rounded hover:bg-blue-400 hover:bg-gray-900">
              Contact Us
            </a>
          </Link>

          {loggedIn && <Link href="/profile">
            <a className="lg:inline-flex lg:w-auto px-3 py-2 rounded hover:bg-blue-500 hover:bg-gray-900">
              {data.email}
            </a>
          </Link>}

          {!loggedIn && <Link href="/signIn">
            <a className="lg:inline-flex lg:w-auto px-3 py-2 rounded hover:bg-blue-500 hover:bg-gray-900">
              Login
            </a>
          </Link>}
          
        </div>
      </div>
    </header>
  );
};

export default Navbar;
