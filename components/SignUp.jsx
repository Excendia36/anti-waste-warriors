import React, { useState } from "react";
import Router from "next/router";
import cookie from "js-cookie";

function SignUp() {
  const [signupError, setSignupError] = useState("");
  const [first_name, setFirstname] = useState("");
  const [last_name, setLastname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [street, setStreet] = useState("");
  const [house_number, setHouseNumber] = useState("");
  const [city, setCity] = useState("");
  const [country, setCountry] = useState("");
  const [user_type, setUserType] = useState("");

  function checkEmpty(input) {
    if (input.length < 1) return true;
    return false;
  }

  function handleSubmit(e) {
    if (checkEmpty(first_name)) return;
    if (checkEmpty(last_name)) return;
    if (checkEmpty(email)) return;
    if (checkEmpty(password)) return;
    if (checkEmpty(street)) return;
    if (checkEmpty(house_number)) return;
    if (checkEmpty(city)) return;
    if (checkEmpty(country)) return;
    if (checkEmpty(user_type)) return;

    e.preventDefault();
    fetch("/api/users", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        first_name,
        last_name,
        email,
        password,
        street,
        house_number,
        city,
        country,
        user_type,
      }),
    })
      .then((r) => r.json())
      .then((data) => {
        if (data && data.error) {
          setSignupError(data.message);
        }
        if (data && data.token) {
          //set cookie
          cookie.set("token", data.token, { expires: 2 });
          Router.push("/");
        }
      });
  }
  return (
    <form onSubmit={handleSubmit}>
      <div className="p-10 m-6">
        <div className="flex flex-wrap pt-10">
          <div className="w-full md:w-1/2">
            <label className="block mb-1" htmlFor="formGridCode_name">
              First name
            </label>
            <input
              className="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
              type="text"
              name="first_name"
              id="formGridCode_name"
              placeholder="John"
              value={first_name}
              onChange={(e) => setFirstname(e.target.value)}
              required={true}
            />
          </div>
          <div className="w-full px-2 md:w-1/2">
            <label className="block mb-1" htmlFor="formGridCode_last">
              Last name
            </label>
            <input
              className="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
              type="text"
              name="last_name"
              id="formGridCode_last"
              placeholder="Doe"
              value={last_name}
              onChange={(e) => setLastname(e.target.value)}
              required={true}
            />
          </div>
        </div>
        <label className="block">
          <span className="text-gray-700 block mt-2">Email</span>
          <input
            className="form-input mt-2 block w-full border rounded-lg focus:shadow-outline"
            type="email"
            name="email"
            placeholder="jane@doe.de"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required={true}
          />
        </label>
        <div className="text-gray-700">
          <label className="block mb-1 mt-2" htmlFor="forms-helpTextCode">
            Password
          </label>
          <input
            className="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
            type="password"
            name="password"
            id="forms-helpTextCode"
            aria-describedby="passwordHelp"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            minLength={6}
            required={true}
          />
          <span className="text-xs text-gray-600" id="passwordHelp">
            Your password must be at least 6 characters long.
          </span>
        </div>
        <div className="flex flex-wrap -mx-2 space-y-4 md:space-y-0">
          <div className="w-full px-2 md:w-1/2">
            <label className="block mb-1 mt-2" htmlFor="formGridCode_name">
              Street
            </label>
            <input
              className="w-full h-10 px-3 text-base placeholder-gray-300 border rounded-lg focus:shadow-outline"
              type="text"
              name="street"
              id="formGridCode_name"
              value={street}
              onChange={(e) => setStreet(e.target.value)}
              required={true}
            />
          </div>
          <div className="flex flex-wrap -mx-2 space-y-4 md:space-y-0">
            <div className="w-full px-2 md:w-1/2">
              <label className="block mb-1 mt-2" htmlFor="formGridCode_name">
                Housenumber
              </label>
              <input
                className="w-full h-10 px-3 text-base placeholder-gray-300 border rounded-lg focus:shadow-outline"
                type="text"
                name="house_number"
                id="formGridCode_name"
                value={house_number}
                onChange={(e) => setHouseNumber(e.target.value)}
                required={true}
              />
            </div>
            <div className="w-full px-2 md:w-1/2">
              <label className="block mb-1 mt-2" htmlFor="formGridCode_last">
                City
              </label>
              <input
                className="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
                type="text"
                name="city"
                id="formGridCode_last"
                value={city}
                onChange={(e) => setCity(e.target.value)}
                required={true}
              />
            </div>
            <div className="w-full px-2 md:w-1/2">
              <label className="block mb-1 mt-2" htmlFor="formGridCode_last">
                Country
              </label>
              <input
                className="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
                type="text"
                name="country"
                id="formGridCode_last"
                value={country}
                onChange={(e) => setCountry(e.target.value)}
                required={true}
              />
            </div>
          </div>
          <div className="mt-4">
            <span className="text-gray-700 block mt-2">I am a(n):</span>
            <div className="mt-2">
              <label className="inline-flex items-center flex justify-center mb-6">
                <input
                  type="radio"
                  className="form-radio border rounded-lg border-gray-400 focus:shadow-outline"
                  name="info"
                  value="charity"
                  onChange={(e) => setUserType(e.target.value)}
                  required={true}
                />
                <span className="ml-2">Charity Organization</span>
              </label>
              <label className="inline-flex items-center ml-6">
                <input
                  type="radio"
                  className="form-radio border rounded-lg border-gray-400 focus:shadow-outline"
                  name="info"
                  value="restaurant"
                  onChange={(e) => setUserType(e.target.value)}
                  required={true}
                />
                <span className="ml-2">Restaurant</span>
              </label>
              <label className="inline-flex items-center ml-6">
                <input
                  type="radio"
                  className="form-radio border border-gray-400 rounded-lg focus:shadow-outline"
                  name="info"
                  value="volunteer"
                  onChange={(e) => setUserType(e.target.value)}
                  required={true}
                />
                <span className="ml-2">Volunteer</span>
              </label>
            </div>
            <button
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                type="submit"
            >
              Sign Up
            </button>
          </div>
        </div>
      </div>

      <br />
      {signupError && <p style={{ color: "red" }}>{signupError}</p>}
    </form>
  );
}

export default SignUp;
